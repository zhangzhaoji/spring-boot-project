package com.zhou.project.user.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-26 16:38
 * @description: [用户实体对象]
 */
@Data
public class User {
    /** 用户主键 */
    private int id;
    /** 用户姓名 */
    private String name;
    /** * 用户性别  1:男 2:女  */
    private int gender;
    /** 出生日期 */
    private Date birthday;
    /** 家庭住址 */
    private String address;
}
