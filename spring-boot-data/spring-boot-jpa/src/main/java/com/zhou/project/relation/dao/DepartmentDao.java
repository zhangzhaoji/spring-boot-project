package com.zhou.project.relation.dao;

import com.zhou.project.relation.entity.Department;
import com.zhou.project.relation.entity.Employees;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:18
 * @description: [部门数据层接口]
 */
public interface DepartmentDao  extends JpaRepository<Department, Integer> {

}
