package com.zhou.project;

import com.zhou.project.simple.entity.Users;
import com.zhou.project.simple.service.UsersService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.util.*;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 10:59
 * @description: Mybatis对象关系单元测试
 */
@SpringBootTest
public class JpaSimpleTests {

    @Autowired
    private UsersService usersService;

    /**
     * 测试 添加 人员
     */
    @Test
    public void insert()
    {
        Users entity = new Users();
        entity.setName("小李子");
        entity.setGender(1);
        entity.setBirthday(new Date());
        entity.setAddress("北京市海淀区");

        usersService.saveOrUpdate(entity);
    }

    /**
     * 测试 修改 人员
     */
    @Test
    public void update()
    {
        Users entity = new Users();
        entity.setId(4);
        entity.setName("小李子1");
        entity.setGender(2);
        entity.setBirthday(new Date());
        entity.setAddress("北京市海淀区1");

        usersService.saveOrUpdate(entity);
    }

    /**
     * 测试 删除 人员
     */
    @Test
    public void delete()
    {
        int id = 4;
        usersService.delete(id);
    }

    /**
     * 测试 批量删除 人员
     */
    @Test
    public void deletes()
    {
        Collection<Integer> ids = List.of(1, 2, 3, 4, 5);
        usersService.deletes(ids);
    }

    /**
     * 测试 主键 查询 人员
     */
    @Test
    public void load()
    {
        Users entity = usersService.queryId(5);
        System.out.println(entity);
    }

    /**
     * 测试 分页 查询 人员
     */
    @Test
    public void page()
    {
        //调用查询方法
        Page<Users> page = usersService.queryPage(0, 10);
        System.out.println("[结果集]:"+page.getContent());
        System.out.println("[总页码]:"+page.getTotalPages());
        System.out.println("[总条数]:"+page.getTotalElements());
        System.out.println("[当前页]:"+page.getNumber());
        System.out.println("[每页条数]:"+page.getSize());
        System.out.println("[本次查询总条数]:"+page.getNumberOfElements());
    }

}
