package com.zhou.project.relation.dao;

import com.zhou.project.relation.entity.Department;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:18
 * @description: [部门数据层接口]
 */
public interface DepartmentDao {
    int insert(Department entity);
    Department loadDepartment(int id);
}
