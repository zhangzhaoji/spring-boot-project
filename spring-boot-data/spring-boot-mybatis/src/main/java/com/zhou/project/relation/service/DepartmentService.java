package com.zhou.project.relation.service;

import com.zhou.project.relation.dao.DepartmentDao;
import com.zhou.project.relation.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:19
 * @description: TODO
 */
@Service
@Transactional(readOnly = true)
public class DepartmentService {

    @Autowired
    private DepartmentDao departmentDao;

    @Transactional
    public int insert(Department entity)
    {
        return departmentDao.insert(entity);
    }

    public Department loadDepartment(int id)
    {
        return departmentDao.loadDepartment(id);
    }

}
