package com.zhou.project.relation.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhou.project.relation.dao.DepartmentDao;
import com.zhou.project.relation.dao.EmployeesDao;
import com.zhou.project.relation.entity.Employees;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:19
 * @description: TODO
 */
@Service
@Transactional(readOnly = true)
public class EmployeesService extends ServiceImpl<EmployeesDao, Employees> {

    @Autowired
    private DepartmentDao departmentDao;

    /**
     * 保证逻辑层同一事务单元 ACID 原子性
     * Creating a new SqlSession [创建全新SqlSession]
     *      Registering transaction synchronization for SqlSession [为SqlSession注册事务同步]
     *          insert into department
     *          Releasing transactional SqlSession [为SqlSession注册事务同步]
     *      Fetched SqlSession from current transaction [为SqlSession注册事务同步]
     *          insert into employees
     *          Releasing transactional SqlSession [为SqlSession注册事务同步]
     *      Transaction synchronization committing SqlSession [事务同步提交SqlSession]
     *      Transaction synchronization deregistering SqlSession [事务同步注销SqlSession]
     * Transaction synchronization closing SqlSession [事务同步关闭SqlSession]
     * @param entity 员工对象(包含部门)
     * @return 响应条数
     */
    @Transactional
    public int insert(Employees entity)
    {
        departmentDao.insert(entity.getDepartment());
        return baseMapper.insert(entity);
    }

}
