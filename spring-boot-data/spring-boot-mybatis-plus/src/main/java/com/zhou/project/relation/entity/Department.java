package com.zhou.project.relation.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:13
 * @description: [部门实体对象]
 */
@Getter
@Setter
@ToString
@TableName(value = "department", resultMap = "departmentResultMap")
public class Department {
    /**
     * 部门主键
     */
    @TableId(type = IdType.AUTO)
    private int departmentId;
    /**
     * 部门名称
     */
    @TableField
    private String departmentName;
    /**
     * 部门位置
     */
    @TableField
    private String departmentLocation;
    /**
     * 部门下属员工
     */
    @TableField(exist = false)
    private List<Employees> employeesList;
    /**
     * 插入时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Timestamp createdTime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private Timestamp updatedTime;
}
