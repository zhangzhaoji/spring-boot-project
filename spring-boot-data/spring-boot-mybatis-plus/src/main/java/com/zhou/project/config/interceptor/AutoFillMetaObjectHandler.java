package com.zhou.project.config.interceptor;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-01 12:19
 * @description: [公共字段内容自动填充]
 */
@Component
public class AutoFillMetaObjectHandler implements MetaObjectHandler {
    /**
     * 添加自动填充
     * @param metaObject 添加对象
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName("createdTime", new Timestamp(System.currentTimeMillis()), metaObject);
    }

    /**
     * 修改自动填充
     * @param metaObject 修改对象
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updatedTime", new Timestamp(System.currentTimeMillis()), metaObject);
    }

}
