package com.zhou.project.user.controller;

import com.zhou.project.user.entity.User;
import org.springframework.web.bind.annotation.*;

/**
 * 用户接口
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-03 16:32
 * @description: TODO
 */
@RestController
@RequestMapping("user")
public class UserController {

    /**
     * 用户注册
     * @param entity 用户属性
     */
    @PostMapping("save")
    public void save(@RequestBody User entity)
    {
    }

    /**
     * 用户编辑
     * @param entity 用户属性
     */
    @PutMapping("update")
    public void update(@RequestBody User entity)
    {
    }

}
