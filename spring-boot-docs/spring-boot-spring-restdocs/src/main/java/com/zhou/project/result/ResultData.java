package com.zhou.project.result;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-04 16:35
 * @description: TODO
 */
@Getter
@Setter
public class ResultData {
    private Integer code;
    private String message;
    private Object data;

    public ResultData() {}

    public ResultData(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 成功 | 无参
     * @return ResultData
     */
    public static ResultData success() {
        ResultData result = new ResultData();
        result.setResultCode(ResultEnum.INTERFACE_CALL_SUCCESS);
        return result;
    }

    /**
     * 成功 | 带参
     * @return ResultData
     */
    public static ResultData success(Object data) {
        ResultData result = new ResultData();
        result.setResultCode(ResultEnum.INTERFACE_CALL_SUCCESS);
        result.setData(data);
        return result;
    }

    /**
     * 自定义成功 | 带参
     * @return ResultData
     */
    public static ResultData success(ResultEnum resultCode) {
        ResultData result = new ResultData();
        result.setResultCode(resultCode);
        return result;
    }

    /**
     * 自定义成功 | 带参
     * @return ResultData
     */
    public static ResultData success(ResultEnum resultCode, Object data) {
        ResultData result = new ResultData();
        result.setResultCode(resultCode);
        result.setData(data);
        return result;
    }

    /**
     * 错误信息 | 无参
     * @return ResultData
     */
    public static ResultData failure() {
        ResultData result = new ResultData();
        result.setResultCode(ResultEnum.INTERFACE_CALL_ERROR);
        return result;
    }

    /**
     * 错误信息 | 无参
     * @return ResultData
     */
    public static ResultData failure(ResultEnum resultCode) {
        ResultData result = new ResultData();
        result.setResultCode(resultCode);
        return result;
    }

    /**
     * 错误信息 | 带参
     * @return ResultData
     */
    public static ResultData failure(ResultEnum resultCode, Object data) {
        ResultData result = new ResultData();
        result.setResultCode(resultCode);
        result.setData(data);
        return result;
    }

    public void setResultCode(ResultEnum code) {
        this.code = code.getCode();
        this.message = code.getMessage();
    }

}
