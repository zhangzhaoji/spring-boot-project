package com.zhou.project;

import org.apache.shiro.authc.MergableAuthenticationInfo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-07 16:21
 * @description: TODO
 */
@SpringBootApplication
public class ShiroPageApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShiroPageApplication.class, args);
    }
}
