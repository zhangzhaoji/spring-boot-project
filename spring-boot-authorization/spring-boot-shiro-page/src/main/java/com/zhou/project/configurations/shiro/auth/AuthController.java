package com.zhou.project.configurations.shiro.auth;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-07 10:58
 * @description: TODO
 */
@Controller
@RequestMapping("auth")
public class AuthController {

    @RequestMapping("unlog")
    public String unlogin()
    {
        return "login";
    }

    @RequestMapping("logout")
    public String logout()
    {
        SecurityUtils.getSubject().logout();
        return "redirect:/auth/unlog";
    }

    @RequestMapping("login")
    public String login(String username, String password)
    {
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, password);
        try {
            SecurityUtils.getSubject().login(usernamePasswordToken);
            return "index";
        }
        catch (Exception exception)
        {
            return "login";
        }
    }

    @RequestMapping("index")
    public String index()
    {
        return "index";
    }
}
