package com.zhou.project.configurations.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-07 10:45
 * @description: TODO
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {

    /**
     * 创建Spring Security密码加密实例
     * @return BCryptPasswordEncoder
     */
    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder();
    }

    /**
     * 配置Spring Security认证信息
     * @param http HttpSecurity
     * @throws Exception 抛出异常
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            // 允许页面中iframe访问
            .headers().frameOptions().sameOrigin()
            .and()
            // 登陆相关配置
            .formLogin()
                // 未登录跳转页面
                .loginPage("/auth/login")
                // 登录跳转跳转页面
                .defaultSuccessUrl("/auth/index")
            .and()
            // 授权认证配置
            .authorizeRequests()
                // 登陆页面放行
                .antMatchers("/auth/login").permitAll()
                // 静态资源放行
                .antMatchers("/css/**", "/img/**", "/webjars/**").permitAll()
                // 其他一律认证
                .anyRequest().authenticated()
            .and()
            // 退出相关配置
            .logout()
                // 退出访问路径
                .logoutUrl("/auth/logout")
                // 退出成功删除指定名称cookie
                .deleteCookies("JSESSIONID")
                // 立即销毁服务器端session
                .invalidateHttpSession(true);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
            // 内存中模拟用户信息
            .inMemoryAuthentication()
                //账号: admin
                //密码: 123456
                //角色: ROLE_ADMIN
                .withUser("admin")
                    .password("$2a$10$NJFjyukKp57.jHVzcO6rKud.j4W/pSTcRMbc69PgbjFDYhTS4liI2")
                    .roles("ADMIN", "USER_INSERT", "USER_DELETE", "USER_UPDATE", "USER_SELECT")
                .and()
                //账号: guest
                //密码: 123456
                //角色: ROLE_USERS
                .withUser("guest")
                    .password("$2a$10$tGl/fDjcqLYJpkbZpSOYVuBUGaaNTx2cmbeB56WNhxquHrcRUpZwW")
                    .roles("USERS", "USER_SELECT");
    }

}
